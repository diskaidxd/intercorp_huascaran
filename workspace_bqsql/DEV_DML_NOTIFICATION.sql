/*
 Autor: John 
 Create date: 22/07/2021
 Description: Script de creacion de Poblado Envio de correos
Type: Data Manipulation Languague (DML)
 */

INSERT INTO c_notif_group_type (id, name, description, flag_active, creation_date, creation_user)
VALUES(1, 'GOVERNANCE', 'Notification for Governance', 'VERDADERO', '2021-05-25 03:42:14', 'prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com'),
      (2, 'OPERATIONAL', 'Notification for Operational', 'VERDADERO', '2021-05-25 03:42:14', 'prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com'),
      (3, 'MANAGER', 'Notification for Manage', 'VERDADERO', '2021-05-25 03:42:14', 'prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com');
	  
INSERT INTO c_notif_group (id, code, name, flag_active, creation_date, creation_user)
VALUES(1, 'G0001', 'GRUPO_PROYECTO_IDEN_PARTY', 'VERDADERO', '2021-05-25 03:42:14', 'prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com'),
      (2, 'G0002', 'GRUPO_PROYECTO_CLIENTE_UNICO', 'VERDADERO', '2021-05-25 03:42:14', 'prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com'),
      (3, 'G0003', 'GRUPO_PROYECTO_CONSUMO_EMPLEADO', 'VERDADERO', '2021-05-25 03:42:14', 'prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com');
	  
INSERT INTO c_user (id, fullname, email, flag_active, creation_date, creation_user)
VALUES(1, 'Mauro Leon', 'mauro.leon@inside.pe', 'VERDADERO', '2021-05-25 03:42:14','prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com'),
      (2, 'Juan Mendoza', 'juan.mendoza@inside.pe', 'VERDADERO', '2021-05-25 03:42:14','prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com'),
      (3, 'John Quispe', 'joh.quispe@inside.pe', 'FALSE', '2021-05-25 03:42:14', 'prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com');
	  
INSERT INTO m_notification_detail_control (id, user_id, notification_id, flag_active, creation_date, creation_user)
VALUES(1, 1, 1, 'VERDADERO', '2021-05-25 03:42:14', 'prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com'),
      (2, 1, 1, 'VERDADERO', '2021-05-25 03:42:14', 'prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com'),
      (3, 1, 1, 'VERDADERO', '2021-05-25 03:42:14', 'prd-itc-dp-matillion-integrati@silent-matter-270218.iam.gserviceaccount.com');
	  
INSERT INTO m_notification_control (id, code, process_code, name, notification_type_id, notification_group_id, flag_active,creation_date)
VALUES(1, 'ALRT001', 'PROC0001', 'NOTIFICACION - PROCESO CLIENTE UNICO', 2, 1, 'VERDADERO', '2021-05-25 03:42:14'),
      (2, 'ALRT002', 'PROC0002', 'NOTIFICACION - PROCESO IDEN PARTY ITC', 1, 2, 'VERDADERO', '2021-05-25 03:42:14'),
      (3, 'ALRT003', 'PROC0003', 'NOTIFICACION - PROCESO EMPLEADO CONSUMO ITC', 2, 3, 'VERDADERO', '2021-05-25 03:42:14');