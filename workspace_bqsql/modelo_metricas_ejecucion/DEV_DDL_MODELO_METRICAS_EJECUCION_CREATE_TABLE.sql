/*
	Description: Script de creacion de tabla c_table_unique
	Proyect: dev-intercorp-data-storage
	Type: Data Definition Languague (DDL)
*/
CREATE TABLE IF NOT EXISTS `dev-intercorp-data-storage.metadata_technical.c_table_unique`
(
	id STRING OPTIONS (description = "Identificador unico de proceso de zona"),
    project_name STRING OPTIONS (description = "Nombre del proyecto GCP donde esta alojado la tabla"),
    dataset_name STRING OPTIONS (description = ""),
    table_name STRING OPTIONS (description = "Nombre de tabla"),
    columna_name STRING OPTIONS (description = ""),
    flag_active STRING OPTIONS (description = "Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)"),
    creation_user DATETIME OPTIONS (description = "Nombre del usuario que crea el registro"),
    creation_date DATETIME OPTIONS (description = "Fecha y hora de creación del registro"),
    update_user STRING OPTIONS (description = "Nombre del usuario que actualiza el registro (flag_active, flag_reprocess, process_interval)"),
    update_date STRING OPTIONS (description = "Fecha y hora de actualización del registro  (flag_active, flag_reprocess, process_interval)")
)
OPTIONS (
	description = "",
	friendly_name = "",
	labels=[("environment","dev"),("company",""), ("project","vuc"), ("purpose",""), ("responsable","jquispelu")]
);

/*
	Description: Script de creacion de tabla c_technical_parameter
	Proyect: dev-intercorp-data-storage 
	Type: Data Definition Languague (DDL)
*/
CREATE TABLE IF NOT EXISTS `dev-intercorp-data-storage.metadata_technical.c_technical_parameter`
(
	id STRING OPTIONS (description = "Identificador unico del registro. UUID"),
    table_type_code STRING OPTIONS (description = "Tipo de tabla"),
    table_type_name STRING OPTIONS (description = "Descripcion de tipo de tabla"),
    interval_type STRING OPTIONS (description = "upper, middle, lower"),
    interval_percent NUMERIC OPTIONS (description = "Porcentaje del umbral"),
    interval_name STRING OPTIONS (description = "Nombre del umbral"),
    interval_color STRING OPTIONS (description = "Código de color del umbral"),
    flag_active STRING OPTIONS (description = "Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)"),
    creation_user DATETIME OPTIONS (description = "Nombre del usuario que crea el registro"),
    creation_date DATETIME OPTIONS (description = "Fecha y hora de creación del registro"),
    update_user STRING OPTIONS (description = "Nombre del usuario que actualiza el registro (flag_active, flag_reprocess, process_interval)"),
    update_date STRING OPTIONS (description = "Fecha y hora de actualización del registro  (flag_active, flag_reprocess, process_interval)")
)
OPTIONS (
	description = "Tabla de parametros de la ejecución de reglas técnicas",
	friendly_name = "Tabla de parametros de la ejecución de reglas técnicas",
	labels=[("environment","dev"),("company",""), ("project","vuc"), ("purpose",""), ("responsable","jquispelu")]
);

/*
	Description: Script de creacion de tabla t_technical_rule_process
	Proyect: dev-intercorp-data-storage 
	Type: Data Definition Languague (DDL)
*/
CREATE TABLE IF NOT EXISTS `dev-intercorp-data-storage.metadata_technical.t_technical_rule_process`
(
	id STRING OPTIONS (description = "Identificador unico del registro. UUID"),
    load_execution_id STRING OPTIONS (description = "Identificador unico de ejecución de job. (run_id)"),
    process_date STRING OPTIONS (description = "Fecha y hora de ejecución de reglas técnicas"),
    itc_company_id STRING OPTIONS (description = ""),
    project_name STRING OPTIONS (description = ""),
    dataset_name STRING OPTIONS (description = ""),
    table_name STRING OPTIONS (description = ""),
    table_type STRING OPTIONS (description = ""),
    dimension_type STRING OPTIONS (description = "Valores posibles (unicidad, completitud)"),
    technical_ruler_code STRING OPTIONS (description = ""),
    metric_name STRING OPTIONS (description = ""),
    metric_value STRING OPTIONS (description = "Acepta valores númericos, integer"),
    metric_read_value STRING OPTIONS (description = ""),
    metric_write_value STRING OPTIONS (description = ""),
    creation_user DATETIME OPTIONS (description = "Nombre del usuario que crea el registro"),
    creation_date DATETIME OPTIONS (description = "Fecha y hora de creación del registro")
)
OPTIONS (
	description = "",
	friendly_name = "",
	labels=[("environment","dev"),("company",""), ("project","vuc"), ("purpose",""), ("responsable","jquispelu")]
);