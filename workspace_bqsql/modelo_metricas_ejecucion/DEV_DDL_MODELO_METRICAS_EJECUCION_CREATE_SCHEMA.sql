/*
 Autor: John  Quispe
 Create date: 05/08/2021
 Description: Script de creacion de Dataset metadata_technical
Type: Data Definition Languague (DDL)
 */

CREATE SCHEMA IF NOT EXISTS `dev-intercorp-data-storage.metadata_technical`
OPTIONS (
	description = "Dataset metada technical",
	labels = [("environment","dev"),("project","vuc"), ("company",""), ("owner",""), ("purpose",""), ("responsable","jquispelu")]
)

