/*
 Autor: John 
 Create date: 05/07/2021
 Description: Script de creacion de Dataset business_huascaran
Type: Data Definition Languague (DDL)
 */

CREATE SCHEMA IF NOT EXISTS `prd-itc-data-storage-pv.business_huascaran`
OPTIONS (
	description = "Dataset Busines de Intercorp",
	labels = [("environment","prd"),("project","huascaran"), ("company","itc"), ("owner",""), ("purpose","education"), ("responsable","jquispelu")]
)
