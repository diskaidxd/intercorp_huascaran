/*
 Autor: John 
 Create date: 22/07/2021
 Description: Script de creacion de Dataset Envio de correos
Type: Data Definition Languague (DDL)
 */

CREATE DATABASE IF NOT EXISTS db_itc_dp_metadata;

USE db_itc_dp_metadata;

CREATE TABLE c_notif_group_type
(
id INT NOT NULL COMMENT 'Identificador de grupo de usuario de notificación',
name varchar(40) COMMENT 'Nombre del grupo de usuario',
description varchar(250) COMMENT 'Descripcion del nombre del grupo de usuario',
flag_active varchar(20) COMMENT 'Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)',
creation_date datetime COMMENT 'Fecha y hora de creación del registro',
creation_user varchar(100) COMMENT 'Nombre del usuario que crea el registro',
PRIMARY KEY (id)
)ENGINE = INNODB;

CREATE TABLE c_notif_group
(
id INT NOT NULL COMMENT 'Identificador de grupo de usuario de notificación',
code varchar(20) COMMENT 'Codigo de notificacion del grupo',
name varchar(40) COMMENT 'Nombre del grupo de proyecto',
flag_active varchar(20) COMMENT 'Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)',
creation_date datetime COMMENT 'Fecha y hora de creación del registro',
creation_user varchar(100) COMMENT 'Nombre del usuario que crea el registro',
PRIMARY KEY (id)
)ENGINE = INNODB;

CREATE TABLE c_user
(
id INT NOT NULL COMMENT 'Identificador de asignación de usuario para notificación',
fullname varchar(40) COMMENT 'Nombre completo del usuario',
email varchar(100) COMMENT 'Correo de electronico del usuario',
flag_active varchar(20) COMMENT 'Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)',
creation_date datetime COMMENT 'Fecha y hora de creación del registro',
creation_user varchar(100) COMMENT 'Nombre del usuario que crea el registro',
PRIMARY KEY (id)
)ENGINE = INNODB;

CREATE TABLE m_notification_detail_control
(
id INT NOT NULL COMMENT 'Identificador unico de componente notificable',
user_id int COMMENT 'Identificador de asignación de usuario para notificación de c_user',
notification_id int COMMENT 'Identificador unico de componente notificable de m_notification_detail_control',
flag_active varchar(20) COMMENT 'Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)',
creation_date datetime COMMENT 'Fecha y hora de creación del registro',
creation_user varchar(100) COMMENT 'Nombre del usuario que crea el registro',
PRIMARY KEY (id),
FOREIGN KEY (user_id) REFERENCES c_user (id),
FOREIGN KEY (notification_id) REFERENCES m_notification_detail_control (id)
)ENGINE = INNODB;



CREATE TABLE m_notification_control
(
id INT NOT NULL COMMENT 'Identificador unico de componente notificable',
code varchar(20) COMMENT 'Codigo de notificacion de componente',
process_code varchar(20) NOT NULL COMMENT 'Codigo de proceso de carga de datos',
name varchar(100) COMMENT 'Nombre de la notificacion',
notification_type_id int COMMENT 'Identificador de grupo de usuario de notificación de c_notif_group_type',
notification_group_id int COMMENT 'Identificador de grupo de usuario de notificación de c_notif_group ',
flag_active varchar(20) COMMENT 'Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)',
creation_date datetime COMMENT 'Fecha y hora de creación del registro',
PRIMARY KEY (id),
FOREIGN KEY (notification_type_id) REFERENCES c_notif_group_type (id),
FOREIGN KEY (notification_group_id) REFERENCES c_notif_group (id)
)ENGINE = INNODB;
