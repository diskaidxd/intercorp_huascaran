/*
 Autor: John  Quispe
 Create date: 05/08/2021
 Description: Script de creacion de Dataset bi_itc_empleado
Type: Data Definition Languague (DDL)
 */

CREATE SCHEMA IF NOT EXISTS `dev-itc-data-storage.bi_itc_empleado`
OPTIONS (
	description = "Dataset Busines de UCIC, Somos Intercorp",
	labels = [("environment","dev"),("project","ucic"), ("company","ucic"), ("owner",""), ("purpose","education"), ("responsable","jquispelu")]
)

