INSERT INTO dev-intercorp-data-operation.metadata_control_execution.demo_table (id,nombre, saldo)
VALUES (1, "elias", 500);
INSERT INTO dev-intercorp-data-operation.metadata_control_execution.demo_table (id,nombre, saldo)
VALUES (2, "jonathan", 200);
INSERT INTO dev-intercorp-data-operation.metadata_control_execution.demo_table (id,nombre, saldo)
VALUES (3, "juan", 1500);
INSERT INTO dev-intercorp-data-operation.metadata_control_execution.demo_table (id,nombre, saldo)
VALUES (4, "castillo", 2500);
INSERT INTO dev-intercorp-data-operation.metadata_control_execution.demo_table (id,nombre, saldo)
VALUES (5, "hernando", 4400);
INSERT INTO dev-intercorp-data-operation.metadata_control_execution.demo_table (id,nombre, saldo)
VALUES (6, "porki", 2000);
INSERT INTO dev-intercorp-data-operation.metadata_control_execution.demo_table (id,nombre, saldo)
VALUES (7, "mario", 3500);

INSERT dataset.Inventory (product, quantity)
VALUES('top load washer', 10),
      ('front load washer', 20),
      ('dryer', 30),
      ('refrigerator', 10),
      ('microwave', 20),
      ('dishwasher', 30),
      ('oven', 5)

INSERT INTO `dev-intercorp-data-operation.metadata_control_execution.prueba_table` 
(
    nombre, 
    sum_saldo
)
VALUES ("malasques", 5500)

