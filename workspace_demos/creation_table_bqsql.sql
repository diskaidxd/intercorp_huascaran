/*
	Description: Script de creacion de tabla om_zone_process
	Proyect: dev-intercorp-data-operation
	Type: Data Definition Languague (DDL)
*/
CREATE TABLE IF NOT EXISTS `dev-intercorp-data-operation.metadata_operational.om_zone_process`
(
	id STRING OPTIONS (description = "Identificador unico de proceso de zona"),
	code STRING OPTIONS (description = "Codigo de zona: RAW, MASTER, BUSINESS"),
	component_name STRING OPTIONS (description = "Nombre tecnico del componente del proceso de zona"),
	component_description STRING OPTIONS (description = "Descripcion funcional del componente"),
	scheduler_definition STRING OPTIONS (description = "Expresion CRON de programacion de ejecución de componente"),
	flag_enabled BOOL OPTIONS (description = "Indicador de habilitacion del registro"),
	creation_user  STRING OPTIONS (description = "Codigo de usuario responsable de la creacion del registro"),
	creation_date TIMESTAMP OPTIONS (description = "Fecha y hora de creacion del registro"),
	update_user STRING OPTIONS (description = "Codigo de usuario responsable de la actualizacion del registro"),
	update_date TIMESTAMP OPTIONS (description = "Fecha y hora de actualizacion del registro")
)
OPTIONS (
	description = "Tabla de proceso de zona",
	friendly_name = "Tabla de proceso de zona",
	labels=[("environment","dev"),("company","itc"), ("project","operational"), ("scope", "metadata")]
);

/*
	Description: Script de creacion de tabla om_group_subprocess
	Proyect: dev-intercorp-data-operation
	Type: Data Definition Languague (DDL)
*/
CREATE TABLE IF NOT EXISTS `dev-intercorp-data-operation.metadata_operational.om_group_subprocess`
(
	id STRING OPTIONS (description = "Identificador unico de proceso de zona"),
	process_id STRING OPTIONS (description = "Identificador de proceso de zona asociado"),
	name STRING OPTIONS (description = "Nombre conceptual del grupo de proceso"),
	type_code STRING OPTIONS (description = "Tipo de subproceso: DL, DQ, OPS"),
	component_name STRING OPTIONS (description = "Nombre tecnico del componente del subproceso"),
	component_description STRING OPTIONS (description = "Descripcion funcional del componente del subproceso"),
	flag_enabled BOOL OPTIONS (description = "Indicador de habilitacion del registro"),
	creation_user  STRING OPTIONS (description = "Codigo de usuario responsable de la creacion del registro"),
	creation_date TIMESTAMP OPTIONS (description = "Fecha y hora de creacion del registro"),
	update_user STRING OPTIONS (description = "Codigo de usuario responsable de la actualizacion del registro"),
	update_date TIMESTAMP OPTIONS (description = "Fecha y hora de actualizacion del registro")
)
OPTIONS (
	description = "Tabla de grupos de subprocesos",
	friendly_name = "Tabla de grupos de subprocesos",
	labels=[("environment","dev"),("company","itc"), ("project","operational"), ("scope", "metadata")]
);

/*
	Description: Script de creacion de tabla om_task_transformation
	Proyect: dev-intercorp-data-operation
	Type: Data Definition Languague (DDL)
*/
CREATE TABLE IF NOT EXISTS `dev-intercorp-data-operation.metadata_operational.om_task_transformation`
(
	id STRING OPTIONS (description = "Identificador unico de proceso de zona"),
	process_id STRING OPTIONS (description = "Identificador de proceso de zona asociado"),
	subprocess_id STRING OPTIONS (description = "Identificador de grupo de subproceso asociado"),
	name STRING OPTIONS (description = "Nombre conceptual de la tarea de transformacion"),
	component_name STRING OPTIONS (description = "Nombre tecnico del componente de la tarea"),
	component_description STRING OPTIONS (description = "Descripcion funcional del componente de la tarea"),
	source_code STRING OPTIONS (description = "Codigo del origen de los datos"),
	source_table STRING OPTIONS (description = "Nombre tecnico de la tabla fuente"),
	target_code STRING OPTIONS (description = "Codigo del destino de los datos "),
	target_table STRING OPTIONS (description = "Nombre tecnico de la tabla destino"),
	flag_enabled BOOL OPTIONS (description = "Indicador de habilitacion del registro"),
	creation_user  STRING OPTIONS (description = "Codigo de usuario responsable de la creacion del registro"),
	creation_date TIMESTAMP OPTIONS (description = "Fecha y hora de creacion del registro"),
	update_user STRING OPTIONS (description = "Codigo de usuario responsable de la actualizacion del registro"),
	update_date TIMESTAMP OPTIONS (description = "Fecha y hora de actualizacion del registro")
)
OPTIONS (
	description = "Tabla de tareas de transformacion",
	friendly_name = "Tabla de tareas de transformacion",
	labels=[("environment","dev"),("company","itc"), ("project","operational"), ("scope", "metadata")]
);

/*
	Description: Script de creacion de tabla om_dload_execution
	Proyect: dev-intercorp-data-operation
	Type: Data Definition Languague (DDL)
*/
CREATE TABLE IF NOT EXISTS `dev-intercorp-data-operation.metadata_operational.om_dload_execution`
(
	id STRING OPTIONS (description = "Identificador unico de ejecucion de carga de datos"),
	execution_runid STRING OPTIONS (description = "Codigo de ejecucion transaccional"),
	execution_date DATE OPTIONS (description = "Fecha de ejecucion transaccional"),
	process_name STRING OPTIONS (description = "Nombre de proceso de carga"),
	zone_code STRING OPTIONS (description = "Codigo de zona"),
	group_name STRING OPTIONS (description = "Nombre de grupo de subprocesos"),
	task_name STRING OPTIONS (description = "Nombre de tarea de transformacion"),
	source_code STRING OPTIONS (description = "Codigo del origen de los datos"),
	source_table STRING OPTIONS (description = "Nombre de tabla fuente"),
	target_code STRING OPTIONS (description = "Codigo del destino de los datos"),
	target_table STRING OPTIONS (description = "Nombre de tabla destino"),
	execution_type STRING OPTIONS (description = "Tipo de ejecucion: REGULAR, REPROCESO"),
	execution_status STRING OPTIONS (description = "Nombre de estado de ejecucion: EN EJECUCION, EXITOSO, ERROR, BLOQUEADO"),
	execution_startdate TIMESTAMP OPTIONS (description = "Fecha y hora de inicio de ejecucion"),
	execution_enddate TIMESTAMP OPTIONS (description = "Fecha y hora de finalizacion de ejecucion"),
	creation_user  STRING OPTIONS (description = "Codigo de usuario responsable de la creacion del registro"),
	creation_date TIMESTAMP OPTIONS (description = "Fecha y hora de creacion del registro")
)
PARTITION BY DATE_TRUNC(execution_date, MONTH)
CLUSTER BY execution_runid
OPTIONS (
	description = "Tabla de ejecucion de carga de datos",
	require_partition_filter = true,
	friendly_name = "Tabla de ejecucion de carga de datos",
	labels=[("environment","dev"),("company","itc"), ("project","operational"), ("scope", "metadata")]
);

/*
	Description: Script de creacion de tabla om_parameter_execution
	Proyect: dev-intercorp-data-operation
	Type: Data Definition Languague (DDL)
*/
CREATE TABLE IF NOT EXISTS `dev-intercorp-data-operation.metadata_operational.om_parameter_execution`
(
	id STRING OPTIONS (description = "Identificador unico de parametro de ejecucion"),
	code STRING OPTIONS (description = "Codigo de parametro"),
	value STRING OPTIONS (description = "Valor de parametro"),
	descripcion STRING OPTIONS (description = "Descripcion funcional del parametro"),
	type STRING OPTIONS (description = "Tipo de parametro"),
	company_code STRING OPTIONS (description = "Codigo de compania"),
	flag_enabled BOOL OPTIONS (description = "Indicador de habilitacion del registro"),
	creation_user  STRING OPTIONS (description = "Codigo de usuario responsable de la creacion del registro"),
	creation_date TIMESTAMP OPTIONS (description = "Fecha y hora de creacion del registro"),
	update_user STRING OPTIONS (description = "Codigo de usuario responsable de la actualizacion del registro"),
	update_date TIMESTAMP OPTIONS (description = "Fecha y hora de actualizacion del registro")
)
OPTIONS (
	description = "Tabla de parametros de ejecucion",
	friendly_name = "Tabla de parametros de ejecucion",
	labels=[("environment","dev"),("company","itc"), ("project","operational"), ("scope", "metadata")]
);
