from google.cloud import bigquery
from datetime import datetime
from dateutil.relativedelta import relativedelta
import pytz

def query_config_control_matillion_profiling():      
  client = bigquery.Client()
  job_config = bigquery.QueryJobConfig(allow_large_results=True)
  query="SELECT origin, GREATEST(ifnull(SAFE_DIVIDE(itc_company_id_null,total_rows),0)," + \
  "ifnull(SAFE_DIVIDE(party_type_id_null,total_rows),0), " + \
  "ifnull(SAFE_DIVIDE(hash_id_null,total_rows),0), " + \
  "ifnull(SAFE_DIVIDE(party_id_null,total_rows),0), " + \
  "ifnull(SAFE_DIVIDE(party_type_id_format,total_rows),0), " + \
  "ifnull(SAFE_DIVIDE(party_id_format,total_rows),0), " + \
  "ifnull(SAFE_DIVIDE(hash_id_format,total_rows),0), " + \
  "ifnull(SAFE_DIVIDE(total_repeted_party_id,total_rows),0), " + \
  "ifnull(SAFE_DIVIDE(total_repeted_hash_id,total_rows),0)) AS kpi, total_rows " + \
  "FROM `silent-matter-270218.ds_matillion.v_matillion_data_profiling`"
  
  query_job = client.query(query, job_config=job_config)
  result_process = query_job.result()
  return result_process

def query_config_control_process_table():      
  client = bigquery.Client()
  query_job_date = client.query(
    """
    SELECT a.itc_company_id
    , a.itc_company_abrev
    , a.process_name AS job_name
    , a.business_process_name AS process_name
    , a.table_fullname AS table_name
    , b.error_name
    , b.error_message
    , b.error_message_detail
    FROM `${prm_process_config_project}.${prm_process_config_dataset}.${prm_process_config_table_dq}` a
    INNER JOIN `${prm_process_config_project}.${prm_process_config_dataset}.${prm_process_config_table_error}` b
    ON(b.error_code = a.error_code)
    WHERE a.business_process_name = @business_process_name
    AND a.status = @status
    AND a.error_code = @error_code
    """
    , bigquery.QueryJobConfig(
      query_parameters=[
        bigquery.ScalarQueryParameter('business_process_name', 'STRING', 'Iden Party Intercorp'),
        bigquery.ScalarQueryParameter('status', 'STRING', 'ACTIVE'),
        bigquery.ScalarQueryParameter('error_code', 'STRING', 'E002')
      ]
    )
  )
  result_job_date = query_job_date.result()
  
  return result_job_date

if __name__ == "__main__":
  tz = pytz.timezone('America/Lima')
  start_date = datetime.now(tz)  
  jobList = query_config_control_process_table()
  jobProfilingList = query_config_control_matillion_profiling()
  
  result_msg = '<center><h1 style='"font-size:22px"'> <b>Reporte de ejecucion del proceso - Iden Party Intercorp</h1> </center> '
  result_msg = result_msg + '<h1 style='"font-size:18px"'> <b>Detalle del Perfilamiento - Iden Party</h1> '
  
  pasan=""
  no_pasan=""
  
  for row in jobProfilingList:
    if row["kpi"]< ${prm_profiling_acepted_kpi}:
      pasan=pasan + "{} (KPI: {:.2%}; # de Filas: {:,}) <br/>".format(row["origin"], row["kpi"], row["total_rows"])
    else:
      no_pasan = no_pasan + "{} (KPI: {:.2%}; # de Filas: {:,}) <br/>".format(row["origin"], row["kpi"], row["total_rows"])
  
  if pasan == "":
    result_msg = "Ninguna tabla origen cumple con el umbral de calidad permitido <br/>" + pasan
    
  elif no_pasan == "":
    result_msg = "Todos los origenes cumplen con el umbral de calidad permitido <br/>" + no_pasan
    
  else:
   
    result_msg = result_msg + "<b> Cumplen con el umbral de calidad: </b> <br/>" + pasan +" <br/>"
    result_msg = result_msg + "<b> No cumplen con el umbral de calidad: </b> <br/>" + no_pasan + " <br/>"
    result_msg = result_msg + "El maximo permitido del KPI es {:.2%} <br/>".format(${prm_profiling_acepted_kpi})
  
  result_msg = result_msg + '<h1 style='"font-size:18px"'> <b>Detalle de errores - Iden Party </b></h1> '
  
  if(jobList.total_rows > 0):
    result_msg = result_msg + '<p style='"font-size:15px;"'> Se detectaron errores en las siguiente(s) tabla(s): </p>'
    result_msg = result_msg + '<table><tr> <th>Empresa</th> <th>Proceso Negocio</th> <th>Job Matillion</th> <th>Tabla</th> <th>Error</th> </tr> '
    
    for job in jobList:
      result_msg = result_msg + '<tr><td style='"text-align:center"'>{}</td> <td style='"text-align:center"'>{}</td> <td>{}</td> <td>{}</td> <td>{} (Particion: {})</td> </tr>'.format(job.itc_company_abrev, job.process_name, job.job_name, job.table_name, job.error_message_detail, (start_date - relativedelta(days=1)).strftime("%Y-%m-%d"))
     
    result_msg = result_msg + ' </table> <br/>'
  
  else:
  	result_msg = result_msg + '<p style='"font-size:15px;"'> El proceso finalizo con exito. </p> <br/>'  
  
  result_msg = result_msg + '<b> Fecha/Hora de ejecucion: </b> ' + start_date.strftime("%d-%m-%Y %H:%M:%S")
  
  context.updateVariable('prm_msg_notification', result_msg)