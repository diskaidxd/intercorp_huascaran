from config.constants import *
from googleapiclient.discovery import build
from time import sleep

def create_secret(creds, secret_id,secreto) -> None:
    """
    """
    parent = "projects/{}".format(constantes['pr_piloto'])
    service = build('secretmanager','v1',credentials=creds)
    lista_secretos = service.projects().secrets().list(parent=parent).execute()['secrets']
    lista_secretos = [i['name'] for i in lista_secretos]
    secret_parent = "projects/131735133173/secrets/" + secret_id
    if secret_parent not in lista_secretos:
        secret = service.projects().secrets().create(parent=parent,body={"replication": {"automatic": {}}},secretId=secret_id).execute()
        version = service.projects().secrets().addVersion(parent=secret['name'], body={"payload": {"data": secreto}}).execute()
    else:
        pass
    
def get_secret(creds,id) -> str:


        secret_rev = service.projects().secrets().versions().access(name="projects/{}/secrets/{}/versions/latest".format(constantes['pr_piloto'],id)).execute()
 
    
    return secret_rev
