import requests
import os
headers = {
'Content-Type': 'application/json',
}

def run_api():
# instanceAddress ='34.136.246.76'
 instanceAddress = os.environ["INSTANCE_ADDRESS"]   
# group = 'dev-itc-dp'
 group = os.environ["GROUP"]
# project = 'itc-dp-core'
 project = os.environ["PROJECT"]
# version = 'default'
 version = os.environ["VERSION"]
# job = 'job_demo'
 job = os.environ["JOB"]
# environment = 'dev-itc-dp-core-env'
 environment = os.environ["ENVIRONMENT"]
# user = 'gcp-user'
 user = os.environ["USER"] 
# password = 'PjD1Lhx99+mM'
 password = os.environ["PASSWORD"]
 
 url = 'http://'+str(instanceAddress)+'/rest/v1/group/name/'+str(group)+'/project/name/'+str(project)+'/version/name/'+str(version)+'/job/name/'+str(job)+'/run?environmentName='+str(environment)
 x = requests.post(url, headers=headers, auth = (str(user),str(password)))

 print(url)
 print(x)
 return x
 
run_api()