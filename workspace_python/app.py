import os
import json
from datetime import datetime, timezone
from flask import Flask, jsonify, request
import flask_restful
import pytz
import sqlalchemy
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from google.oauth2 import service_account


app = Flask(__name__)

@app.route("/sendEmail", methods=["POST"])
def sendEmail():
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    
    # Configuracion de variables de entorno

    db_user = os.environ["DB_USER"]
    db_pass = os.environ["DB_PASS"]
    db_name = os.environ["DB_NAME"]
    email_sender = os.environ["EMAIL_SENDER"]
    sendgrid_key = os.environ["SENDGRID_KEY"]    
    cloud_sql_connection_name = os.environ["CLOUD_SQL_CONNECTION_NAME"]
    db_socket_dir = '/cloudsql/{}'.format(cloud_sql_connection_name)
    request_json = request.get_json()

    alert_id = request_json['alert_id']
    component_name = request_json["component_name"]
    component_type = request_json["component_type"]
    success_flag = request_json["success_flag"]
    message = request_json["message"]
    
    # Configuracion de propiedades para la conexion a MySQL

    db_config = {
        # [START cloud_sql_mysql_sqlalchemy_limit]
        # Pool size is the maximum number of permanent connections to keep.
        "pool_size": 5,
        
        # Temporarily exceeds the set pool_size if no connections are available.
        "max_overflow": 2,
        
        # The total number of concurrent connections for your application will be
        # a total of pool_size and max_overflow.
        # [END cloud_sql_mysql_sqlalchemy_limit]

        # [START cloud_sql_mysql_sqlalchemy_backoff]
        # SQLAlchemy automatically uses delays between failed connection attempts,
        # but provides no arguments for configuration.
        # [END cloud_sql_mysql_sqlalchemy_backoff]

        # [START cloud_sql_mysql_sqlalchemy_timeout]
        # 'pool_timeout' is the maximum number of seconds to wait when retrieving a
        # new connection from the pool. After the specified amount of time, an
        # exception will be thrown.
        "pool_timeout": 30,  # 30 seconds
        # [END cloud_sql_mysql_sqlalchemy_timeout]

        # [START cloud_sql_mysql_sqlalchemy_lifetime]
        # 'pool_recycle' is the maximum number of seconds a connection can persist.
        # Connections that live longer than the specified amount of time will be
        # reestablished
        "pool_recycle": 1800,  # 30 minutes
        # [END cloud_sql_mysql_sqlalchemy_lifetime]

    }
    
    # Estableciendo conexion a MySQL

    db = sqlalchemy.create_engine(
        # Equivalent URL:
        # mysql+pymysql://<db_user>:<db_pass>@/<db_name>?unix_socket=<socket_path>/<cloud_sql_instance_name>
        sqlalchemy.engine.url.URL(
            drivername="mysql+pymysql",
            username=db_user,  # e.g. "my-database-user"
            password=db_pass,  # e.g. "my-database-password"
            database=db_name,  # e.g. "my-database-name"
            query={
                "unix_socket": "{}".format(
                    db_socket_dir  # e.g. "/cloudsql"
                    )  
            }
        ),
        **db_config)
    
    # Apertura una conexion y consulta los metadatos usados para enviar correo
        
    with db.connect() as conn:
        query = "select alert_id , list_id , success_msg , fail_msg , success_subject , fail_subject from alert where alert_id='" + alert_id + "' and enabled=1"
        rows = conn.execute(query).fetchall()
        if len(rows) == 1:
            receivers = []
        sender = email_sender
        msg = rows[0][2] if success_flag == "1" else rows[0][3]
        sbj = rows[0][4] if success_flag == "1" else rows[0][5]
        query = "select email from user_list_detail where list_id='" + rows[0][1] + "'"
        emails = conn.execute(query).fetchall()
        if len(emails) > 0:
            for email in emails:
                receivers.append(email[0])
            utc_dt = datetime.now(timezone.utc)
            tz_lima = pytz.timezone('America/Lima')
            execution_date = utc_dt.astimezone(tz_lima).isoformat()
            sbj = sbj.replace("<*EXECUTION_DATE*>", execution_date)
            sbj = sbj.replace("<*COMPONENT_TYPE*>", component_type)
            sbj = sbj.replace("<*COMPONENT_NAME*>", component_name)
            msg = msg.replace("<*EXECUTION_DATE*>", execution_date)
            msg = msg.replace("<*COMPONENT_TYPE*>", component_type)
            msg = msg.replace("<*COMPONENT_NAME*>", component_name)
            msg = msg.replace("<*MESSAGE*>", message)
            message = Mail(
                from_email=sender,
                to_emails=receivers,
                subject=sbj,
                html_content=msg)
            try:
                #Invoca al API de Sendgrid para enviar correo y pasa el Key como parametro
                sg = SendGridAPIClient(sendgrid_key)
                response = sg.send(message)
                #print(response.status_code)
                #print(response.body)
                #print(response.headers)
            except Exception as e:
                print(e.message)

    return "OK"

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))