import json

def cargar_datos(ruta):
    with open(ruta) as contenido:
        cursos = json.load(contenido)
        for curso in cursos:
            print(curso['source_code'])
        
if __name__ == '__main__':
    ruta = 'lista.json'
    cargar_datos(ruta)