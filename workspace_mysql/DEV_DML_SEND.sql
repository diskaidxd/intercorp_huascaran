/*
 Autor: John 
 Create date: 02/08/2021
 Description: Script de creacion de Poblado Envio de correos
Type: Data Manipulation Languague (DML)
 */

INSERT INTO c_notif_channel(id, code, description, flag_active, creation_date, creation_user)
VALUES(1, 'C001', 'CANAL DE NOTIFICACION - CORREO ELECTRONICO', 1, '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net'),
	(2, 'C002', 'CANAL DE NOTIFICACION - SLACK', 0, '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net');
COMMIT;
  
INSERT INTO c_notif_type(id, name, description, gs_template_uri, gs_html_uri, flag_active, creation_date, creation_user)
VALUES(1, 'OPERATIONAL', 'NOTIFICACIONES DEL TIPO OPERACIONAL', 'db_itc_dp_metadata/templates/operational.json', 'db_itc_dp_metadata/html/operational.html',  '1', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net'),
	(2, 'GOVERNANCE', 'NOTIFICACIONES DEL TIPO GOBIERNO DE DATOS', 'db_itc_dp_metadata/templates/governance.json', 'db_itc_dp_metadata/html/governance.html', '1', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net'),      
      (3, 'MANAGER', 'NOTIFICACIONES DEL TIPO ESTRATEGICO', 'db_itc_dp_metadata/templates/management.json', 'db_itc_dp_metadata/html/manager.html' , '1', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net');
COMMIT;
  
INSERT INTO c_notif_group(id, code, name, flag_active, creation_date, creation_user)
VALUES(1, 'G0001', 'GRUPO-DESTINATARIOS-IDEN_PARTY', '1', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net'),
      (2, 'G0002', 'GRUPO-DESTINATARIOS-CLIENTE_UNICO', '1', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net'),
      (3, 'G0003', 'GGRUPO-DESTINATARIOS-CONSUMO_EMPLEADO', '1', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net');
COMMIT;	  
	  
INSERT INTO c_notif_usergroup (id, notification_group_id, email, flag_active, creation_date, creation_user)
VALUES(1, 1, 'mauro.leon@inside.pe', '1', '2021-05-25 03:42:14','jesus.diaz@bluetab.net'),
      (2, 1, 'juan.mendoza@inside.pe', '1', '2021-05-25 03:42:14','jesus.diaz@bluetab.net'),
      (3, 1, 'johh.quispe@inside.pe', '0', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net'),
	(4, 1, 'jesus.diaz@bluetab.net', '1', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net');
COMMIT;

INSERT INTO m_notification_control (id, code, process_code, name, business_process_name, notification_type_id, notification_group_id, channel_id, flag_active, creation_date, creation_user)
VALUES(1, 'ALRT001', 'PROC0001', 'NOTIFICACION - PROCESO IDEN PARTY ITC', 'INTERCORP - IDEN PARTY', 1, 1, 1, '1', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net'),
	(2, 'ALRT002', 'PROC0002', 'NOTIFICACION - PROCESO CLIENTE UNICO', 'INTERCORP - VISTA UNICA DE CLIENTE', 1, 2, 1, '1', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net'),      
      (3, 'ALRT003', 'PROC0003', 'NOTIFICACION - PROCESO EMPLEADO CONSUMO ITC', 'INTERCORP - CONSUMO EMPLEADO', 2, 3, 1, '1', '2021-05-25 03:42:14', 'jesus.diaz@bluetab.net');
COMMIT;
