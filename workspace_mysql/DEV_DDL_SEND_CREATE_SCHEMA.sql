/*
 Autor: John 
 Create date: 02/08/2021
 Description: Script de creacion de Base datos y Tablas de Envio de correos
Type: Data Definition Languague (DDL)
 */

CREATE DATABASE IF NOT EXISTS db_itc_dp_metadata;

USE db_itc_dp_metadata;

DROP TABLE IF EXISTS c_notif_channel;
CREATE TABLE c_notif_channel(
id INT NOT NULL COMMENT 'Identificador del canal de correo',
code VARCHAR(20) COMMENT 'Codigo del canal de la notificacion',
description VARCHAR(300) COMMENT 'Descripcion del canal de notificacion',
flag_active CHAR(1) COMMENT 'Flag que indica el estado del canal (1: ACTIVE | 0:INACTIVE)',
creation_date DATETIME COMMENT 'Fecha y hora de creación del registro',
creation_user VARCHAR(100) COMMENT 'Nombre del usuario que crea el registro',
PRIMARY KEY (id)
)ENGINE = INNODB;

DROP TABLE IF EXISTS c_notif_type;
CREATE TABLE c_notif_type
(
id INT NOT NULL COMMENT 'Identificador de grupo de usuario de notificación',
name VARCHAR(50) COMMENT 'Nombre del grupo de usuario',
description VARCHAR(250) COMMENT 'Descripcion del nombre del grupo de usuario',
gs_template_uri VARCHAR(4000) COMMENT 'URI del archivo template almacenado en el bucket',
gs_html_uri VARCHAR(4000) COMMENT 'URI del archivo html que lleva mensaje de la notificacion almacenado en el bucket',
flag_active CHAR(1) COMMENT 'Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)',
creation_date DATETIME COMMENT 'Fecha y hora de creación del registro',
creation_user VARCHAR(100) COMMENT 'Nombre del usuario que crea el registro',
PRIMARY KEY (id)
)ENGINE = INNODB;

DROP TABLE IF EXISTS c_notif_group;
CREATE TABLE c_notif_group
(
id INT NOT NULL COMMENT 'Identificador de grupo de usuario de notificación',
code VARCHAR(20) COMMENT 'Codigo de notificacion del grupo',
name VARCHAR(50) COMMENT 'Nombre del grupo de proyecto',
flag_active CHAR(1) COMMENT 'Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)',
creation_date DATETIME COMMENT 'Fecha y hora de creación del registro',
creation_user VARCHAR(100) COMMENT 'Nombre del usuario que crea el registro',
PRIMARY KEY (id)
)ENGINE = INNODB;

DROP TABLE IF EXISTS c_notif_usergroup;
CREATE TABLE c_notif_usergroup
(
id INT NOT NULL COMMENT 'Identificador de grupo de usuarios para notificación',
notification_group_id INT COMMENT 'Identificador de grupo de usuario de notificación de c_notif_group ',
email VARCHAR(100) COMMENT 'Correo de electronico del usuario',
flag_active CHAR(1) COMMENT 'Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)',
creation_date DATETIME COMMENT 'Fecha y hora de creación del registro',
creation_user VARCHAR(100) COMMENT 'Nombre del usuario que crea el registro',
PRIMARY KEY (id),
FOREIGN KEY (notification_group_id) REFERENCES c_notif_group (id)
)ENGINE = INNODB;

DROP TABLE IF EXISTS m_notification_control;
CREATE TABLE m_notification_control
(
id INT NOT NULL COMMENT 'Identificador unico de componente notificable',
code VARCHAR(20) COMMENT 'Codigo de notificacion de componente',
process_code VARCHAR(20) NOT NULL COMMENT 'Codigo de proceso de carga de datos',
name VARCHAR(100) COMMENT 'Nombre de la notificacion',
business_process_name VARCHAR(100) COMMENT 'Proceso del negocio relacionada a la notificacion',
notification_type_id INT COMMENT 'Identificador de tipo de notificación de c_notif_type',
notification_group_id INT COMMENT 'Identificador del grupo de usuarios destinatarios',
channel_id INT COMMENT 'Identificador de canal de la notificacion',
flag_active CHAR(1) COMMENT 'Flag que indica el estado del job (1: ACTIVE | 0:INACTIVE)',
creation_date DATETIME COMMENT 'Fecha y hora de creación del registro',
creation_user VARCHAR(100) COMMENT 'Nombre del usuario que crea el registro',
PRIMARY KEY (id),
FOREIGN KEY (notification_type_id) REFERENCES c_notif_type(id),
FOREIGN KEY (notification_group_id) REFERENCES c_notif_group(id),
FOREIGN KEY (channel_id) REFERENCES c_notif_channel(id)
)ENGINE = INNODB;